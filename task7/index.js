(function () {
  const button = document.querySelector('.button');
  const container = document.querySelector('.container');

  const createRandomNumber = (min, max) => {
    return Math.round(Math.random() * (max - min)  + min);
  }

  const createHSL = () => {
    let r = createRandomNumber(0, 255);
    let g = createRandomNumber(0, 255);
    let b = createRandomNumber(0, 255);

    return 'rgb(' + r + ', ' + g + ', ' + b + ')';
  }

  const createSquare = (parent) => {
    let square = document.createElement('div');
    square.classList.add('square');
    square.style.backgroundColor = createHSL();

    parent.appendChild(square);
  }

  const renderSquares = () => {
    let blockCount = createRandomNumber(10, 100);
    container.innerHTML = '';
    for (let i = 0; i < blockCount; i++) {
      createSquare(container);
    }
  };

  renderSquares();

  button.addEventListener('click', renderSquares);
})();