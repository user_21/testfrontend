(async function main() {

  const cardContainer = document.querySelector('.card-container');

  const url1 = "https://reqres.in/api/users";
  const url2 = "https://reqres.in/api/users?page=2";

  const requestFetch = async (url) => {
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      console.log("Wrong: " + response.status);
    }
  }

  let response = await requestFetch(url1);

  let data = response.data;

  response = await requestFetch(url2);

  data = data.concat(response.data);

  data.sort((a, b) => a.first_name.localeCompare(b.first_name));

  const nav = document.querySelector('.nav-block');
  let pageIndex = 0;
  let itemsPerPage = 6;
  loadItems();

  function loadItems() {
    cardContainer.innerHTML = "";
    let items = data;
    for (let i = pageIndex * itemsPerPage; i < (pageIndex * itemsPerPage) + itemsPerPage; i++) {
      if (!items[i]) { break }

      const child = document.createElement('div');
      cardContainer.appendChild(child);
      child.classList.add("item");

      for (let item in data[i]) {
        if (item != "avatar") {
          const text = document.createElement('span');
          text.innerHTML = item + ": " + data[i][item];
          child.appendChild(text);
        } else {
          const avatar = document.createElement("img");

          avatar.setAttribute("src", data[i][item]);
          avatar.classList.add("img");

          child.appendChild(avatar);
        }
      }
      loadPageNav();
    }
  }


  function loadPageNav() {
    nav.innerHTML = "";
    let items = data;
    for (let i = 0; i < (items.length / itemsPerPage); i++) {
      const span = document.createElement('span');
      span.classList.add("nav-block__span");
      span.innerHTML = i + 1;
      span.addEventListener('click', (e) => {
        pageIndex = e.target.innerHTML - 1;
        loadItems();
      });
      if (i === pageIndex) {
        span.style.background = "rgb(170, 161, 178)";
      }
      nav.append(span);
    }
  }
})();

