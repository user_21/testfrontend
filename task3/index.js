const video = document.querySelector('.video');
const videoTime = document.querySelector('.video-time');
const body = document.body;

isPlayback = false;
begTime = "00:00:000";

videoTime.innerHTML = begTime;

video.ontimeupdate = (evt) => {
  const time = String(evt.target.currentTime);

  const splitTime = time.split(".");
  let sec = splitTime[0];
  let msec = splitTime[1].substring(0, 3);

  let min = '00';

  if (sec >= 60) {
    min = Math.trunc(sec / 60)
    sec = sec - min * 60;
    if (min < 10) {
      min = '0' + min;
    }
  }

  if (sec < 10) {
    sec = '0' + sec;
  }

  videoTime.innerHTML = min + ":" + sec + ":" + msec;
}

const endVideo = () => {
  video.currentTime = 0;
  isPlayback = false;
  videoTime.innerHTML = begTime;
}

video.addEventListener('ended', endVideo);

function play() {
  video.play();
  isPlayback = !isPlayback;
}

function pause() {
  video.pause();
  isPlayback = !isPlayback;
}

const playbackToggle = () => {
  isPlayback ? pause() : play();
}

body.addEventListener('click', playbackToggle);