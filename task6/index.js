const firstName = document.querySelector(".form__input--name");
const phone = document.querySelector(".form__input--phone");
const password = document.querySelector(".form__input--password");
const confPassword = document.querySelector(".form__input--сonf-password");

const correctName = (str) => {
  return (
    /^[a-zA-Zа-яА-Я]+$/.test(str)
    && str.length >= 3
    && str.length <= 30
  );
}

const correctPhoneNumber = (str) => {
  return (
    (/^\d+$/.test(str) || /^\+\d+$/.test(str))
    && str.length >= 10
    && str.length <= 15
  );
}

const correctPassword = (str) => {
  return (
    (/\d/.test(str) && /[A-ZА-Я]/.test(str))
    && str.length >= 8
    && str.length <= 40
  );
}

const formValid = () => {
  return (
    correctName(firstName.value)
    && correctPhoneNumber(phone.value)
    && correctPassword(password.value)
    && (password.value == confPassword.value)
  );
}

falseBorder = "rgb(193, 51, 35) solid 2px";
trueBorder = "rgb(88, 143, 88) solid 2px";

const submits = () => {
  if (formValid()) {
    firstName.style.border = trueBorder;
    phone.style.border = trueBorder;
    password.style.border = trueBorder;
    confPassword.style.border = trueBorder;
    alert("Успешно!");
  } else {
    alert("Ошибка!");
    correctName(firstName.value) ? firstName.style.border = trueBorder : firstName.style.border = falseBorder;
    correctPhoneNumber(phone.value) ? phone.style.border = trueBorder : phone.style.border = falseBorder;
    correctPassword(password.value) ? password.style.border = trueBorder : password.style.border = confPassword.style.border = falseBorder;
  }
}
