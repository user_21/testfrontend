class Slider {
  constructor(slider, items) {
    this.items = items;
    this.slides = document.querySelector(".carousel-slides");
    this.fillHtml();
    this.allFrames = slider.querySelectorAll('.carousel-item');
    this.frameChain = slider.querySelector('.carousel-slides');
    this.nextButton = slider.querySelector('.carousel-next');
    this.prevButton = slider.querySelector('.carousel-prev');
    this.index = 0;
    this.length = this.allFrames.length;
    this.init();
  }

  fillHtml() {

    for (let i = 0; i < items.length; i++) {

      const img = document.createElement("img");

      img.classList.add("carousel-item");

      img.setAttribute("src", `${items[i]}`);

      this.slides.appendChild(img);
    }

  }

  init() {
    this.allFrames.forEach(frame => frame.style.width = 100 / this.length + '%');

    this.frameChain.style.width = 100 * this.length + '%';

    this.nextButton.addEventListener('click', event => {
      event.preventDefault();
      this.next();
    });

    this.prevButton.addEventListener('click', event => {
      event.preventDefault();
      this.prev();
    });
  }

  goto(index) {
    if (index > this.length - 1) {
      this.index = 0;
    } else if (index < 0) {
      this.index = this.length - 1;
    } else {
      this.index = index;
    }
    this.move();
  }

  next() {
    this.goto(this.index + 1);
  }

  prev() {
    this.goto(this.index - 1);
  }

  move() {
    const offset = 100 / this.length * this.index;
    this.frameChain.style.transform = `translateX(-${offset}%)`;

  }
}

const items = [
  'https://stroy-podskazka.ru/images/article/orig/2019/03/kroliki-porody-baran-opisanie-raznovidnosti-i-usloviya-soderzhaniya.jpg',
  'https://placepic.ru/wp-content/uploads/2018/06/108056lpr.jpg',
  'https://klike.net/uploads/posts/2022-09/1662094965_j-13.jpg',
  'https://www.androscogginanimalhospital.com/blog/wp-content/uploads/2017/10/Andro_iStock-506853536.jpg',
  'https://pics.botanichka.ru/wp-content/uploads/2019/12/dekorativnyiy-krolik-01.jpg',
  'https://kartinkin.net/uploads/posts/2022-03/1647048910_22-kartinkin-net-p-kartinki-s-krolikami-22.jpg',
];

document.addEventListener('DOMContentLoaded', () => {
  new Slider(document.querySelector('.carousel'), items);
});