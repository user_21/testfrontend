import * as fs from 'fs';

const inFilePath = "./infile.txt";
const outFilePath = "./outfile.txt";

(async function () {
  const readFile = async (filePath) => {
    try {
      const data = await fs.promises.readFile(filePath, 'utf8');
      return data
    }
    catch (err) {
      console.log(err);
    }
  }

  const content = await readFile(inFilePath);

  const reverseContent = content.split('').reverse().join('');

  fs.writeFile(outFilePath, reverseContent, (err) => {
    if (err) {
      console.error(err);
      return
    }
  });
})();

