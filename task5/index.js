(function () {
  const button = document.querySelector('.button');
  const boxes = document.querySelectorAll('.box');
  let isOpen = false;

  const openBox = (box) => {
    const cover = box.querySelector('.box__cover');
    cover.classList.toggle('box__cover--opened');
    isOpen ? button.innerHTML = "Открыть" : button.innerHTML = "Закрыть";
  }

  const boxOpener = () => {
    for (let i = 0; i < 3; i++) {
      openBox(boxes[i]);
    }
    isOpen = !isOpen;
  }

  function debounce(callback, delay) {
    let timeout;
    return function () {
      clearTimeout(timeout);
      timeout = setTimeout(callback, delay);
    }
  }

  button.addEventListener('click', debounce(boxOpener, 1000));
})();