const circle = document.querySelector('.circle');
const rectangle = document.querySelector('.rectangle');
const triangle = document.querySelector('.triangle');
const square = document.querySelector('.square');

const pulseButton = document.querySelector('.button-pulse');
const rotateButton = document.querySelector('.button-rotate');
const moveButton = document.querySelector('.button-move');
const transformButton = document.querySelector('.button-transform');

let isPulse = false;
let isRotate = false;
let isMove = false;
let isTransform = false;

const addClass = (className) => {
  circle.classList.add(className);
  rectangle.classList.add(className);
  triangle.classList.add(className);
  square.classList.add(className);
}

const removeClass = (className) => {
  circle.classList.remove(className);
  rectangle.classList.remove(className);
  triangle.classList.remove(className);
  square.classList.remove(className);
}

const pulse = () => {
  if (isPulse) {
    removeClass("pulse")
    isPulse = !isPulse;
  } else {
    addClass("pulse")
    isPulse = !isPulse;
  }
}

const rotate = () => {
  if (isRotate) {
    removeClass("rotate")
    isRotate = !isRotate;
  } else {
    addClass("rotate")
    isRotate = !isRotate;
  }
}

const move = () => {
  if (isMove) {
    removeClass("move");
    isMove = !isMove;
  } else {
    addClass("move")
    isMove = !isMove;
  }
}

const transform = () => {
  if (isTransform) {
    removeClass("transform");
    isTransform = !isTransform;
  } else {
    addClass("transform");
    isTransform = !isTransform;
  }
}

pulseButton.addEventListener('click', pulse);
rotateButton.addEventListener('click', rotate);
moveButton.addEventListener('click', move);
transformButton.addEventListener('click', transform);